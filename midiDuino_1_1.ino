#include <shiftLed.h>
#include <I2CIO.h>
#include <LiquidCrystal_SR.h>
#include <LiquidCrystal.h>
#include <LiquidCrystal_I2C.h>
#include <LCD.h>
#include <LiquidCrystal_SR2W.h>
#include <LiquidCrystal_SR3W.h>
#include <FastIO.h>


#include <MemoryFree.h>
#include <SPI.h>
#include <MIDI.h>

#include <Wire.h>
LiquidCrystal_I2C lcd(0x3B);

byte tetris[64]={0,60,0,60,0,55,0,56,0,58,0,58,0,56,0,55,0,53,0,53,0,53,0,56,0,60,0,60,0,58,0,56,0,55,0,55,0,55,0,56,0,58,0,58,0,60,0,60,0,56,0,56,0,53,0,53,0,53,0,53,0,53,0,53};
byte midiChOne=1;
byte midiChTwo=2;
byte midiChThree=3;
byte midiChFour=4;
byte pressed1;
byte pressed2;
byte pressed3;
byte pressed4;
byte midiInCh = 4;
byte SPEED=100;
byte button[16];
byte notes[4][64];
byte value=0; //interface button values 
byte potChanged[7];
bool playChange=1;
bool bpmChange=1;
bool partChange=1;
bool patchChange=1;
bool mainInit=1;
bool menu=0;
bool exitMenu=0;
bool enter=0;
char* menuItem[4]={"MidiCH","Faders","USB","Clock"};
byte currentMenuItem=0;
int bpm;
byte notesStatus[4][64];
bool statusChange=0;
byte currentlyEditting=1; //patch currently editing
byte currentPartEdit = 0;
byte currentlyPlaying[]= {1,1,1,1};
byte partPart = 0;
byte RB[3]; // recieve bits for spi
byte potPositions[7]; // position values of faders
byte justPressed1=0; // var to check if a butt has just been pressed to prevent data spam
byte justPressed2=0; 
byte justPressed3=0; 
byte justPressed4=0;
bool playing = 0;
byte justStarting=1;
byte currentNote;
long waitUntillNote = 0;
long waitUntillRead = 0;
long waitUntillSyncPulse = 0;
long waitUntillUpdate = 0; 
int noteNr=0;
bool transportState = 0;
bool editMode = 0;
int benchmark=0;
shiftLED shiftLed;
void setup()
{
  SPI.begin();
  SPI.setBitOrder(MSBFIRST);
  SPI.setClockDivider(2);  
  pinMode(7, INPUT);
  lcd.begin(16,2);
  lcd.print(freeMemory()); 
  
  MIDI.begin(midiInCh);
  Serial.begin(115200);

  Wire.begin();        // join i2c bus (address optional for master)
  //Serial.begin(9600);  // start serial for output
  //for(int i=0; i<15; i++){
    //notesStatus[i]=0;
  //}
  notes_init();
  MIDI.sendRealTime(Stop);
  //MIDI.sendRealTime(Start);
  startPlayback;
  currentlyPlaying[0]=1;
  currentlyPlaying[1]=1; 
  currentlyPlaying[2]=1;
  currentlyPlaying[3]=1;

}

void loop()
{
 
  convertToBpm();  
  bool noteOn = 0;
  readInput(3); //read and convert transport and part select buttons
  convertInput(pressed3, 3);   
  updateDisplay(); 
 // value=bpm;
  
 // lcd.clear();
 // lcd.print(benchmark);
//  delay(10);
  while(playing==1){
      //sendCC();
      //setBpm();
      //updateDisplay(); 
      getPositions();
      byte tmp=currentlyEditting;
      //currentlyEditting = analogRead(A2)/256+1;
      if(currentlyEditting != tmp){statusChange=1;}
      
      currentPartEdit = analogRead(A0)/256;
      //SPEED = analogRead(A0)/5;
     
      readInput(1); //read and convert seq butts 1-8
      if(editMode==1){setNotes(currentlyEditting);}
      convertInput(pressed1, 1); 
      readInput(2); //read and convert seq butts 9-16
      if(editMode==1){setNotes(currentlyEditting);}
      convertInput(pressed2, 2);
      readInput(3); //read and convert transport and part select buttons
      convertInput(pressed3, 3); 
      readInput(4); //read and convert transport and part select buttons
      convertInput(pressed4, 4);     
      if(editMode==1){setNotes(currentlyEditting);}
      if(editMode==0){clearAfterEdit();}
      if(millis() >= waitUntillUpdate){ // millis loop for outputting to leds
        waitUntillUpdate=millis()+150;
        writeToLeds();}
   
      if(millis() >= waitUntillSyncPulse){ // millis loop for sync playing8 times as fast
        MIDI.sendRealTime(Clock);
        if(millis() >=waitUntillNote) { // millis loop for playing note
           if (noteOn == 1){
              if(justStarting==1){MIDI.sendRealTime(Start);justStarting=0;}
              playNote(noteNr, 0);
              noteNr=noteNr+1;
              updateDisplay();
              noteOn=0;}          
              if (noteOn == 0){
                playNote(noteNr, 1);
                noteOn=1;}
            waitUntillNote=millis()+SPEED;
         }
          byte tempSpeed=SPEED/4;
          waitUntillSyncPulse=millis() + (SPEED-tempSpeed)/4;}
      
      if (noteNr==64){noteNr=0;}
      partPart=noteNr/16;
      if (partPart>3){partPart=0;}
  }
 
}

void readInput(byte buttSet)
{
  
    if(buttSet == 1){
      Wire.requestFrom(0x38, 1);
      byte c = Wire.read(); // receive a byte as character
      if(c < 255) {pressed1 = c;}
      justPressed1 = c;
    }
    if(buttSet == 2){
      Wire.requestFrom(0x39, 1);
      byte c = Wire.read(); // receive a byte as character
      if(c < 255) {pressed2 = c;}
      justPressed2 = c;
    }
    if(buttSet == 3){
      Wire.requestFrom(0x3A, 1);
      byte c = Wire.read(); // receive a byte as character
      if(c < 255) {pressed3 = c;}
      justPressed3 = c;
    }
   if(buttSet == 4){
      Wire.requestFrom(0x3C, 1);
      byte c = Wire.read(); // receive a byte as character
      if(c < 255) {pressed4 = c;}
      justPressed4 = c;
    }

}


void convertInput(byte addr, byte buttSet) // converts button adres to button number 
{
  if(buttSet==1){
    if (justPressed1 != pressed1){
    if(addr == 254){button[1]= !button[1]; pressed1 = 255;}
    if(addr == 253){button[2]= !button[2]; pressed1 = 255;}
    if(addr == 251){button[3]= !button[3]; pressed1 = 255;}
    if(addr == 247){button[4]= !button[4]; pressed1 = 255;}
    if(addr == 239){button[5]= !button[5]; pressed1 = 255;}
    if(addr == 223){button[6]= !button[6]; pressed1 = 255;}
    if(addr == 191){button[7]= !button[7]; pressed1 = 255;}
    if(addr == 127){button[8]= !button[8]; pressed1 = 255;}
    }
  }
  if(buttSet==2){
    if (justPressed2 != pressed2){
    if(addr == 254){button[9]= !button[9]; pressed2 = 255;}
    if(addr == 253){button[10]= !button[10]; pressed2 = 255;}
    if(addr == 251){button[11]= !button[11]; pressed2 = 255;}
    if(addr == 247){button[12]= !button[12]; pressed2 = 255;}
    if(addr == 239){button[13]= !button[13]; pressed2 = 255;}
    if(addr == 223){button[14]= !button[14]; pressed2 = 255;}
    if(addr == 191){button[15]= !button[15]; pressed2 = 255;}
    if(addr == 127){button[16]= !button[16]; pressed2 = 255;}
    }
  }
  if(buttSet==3){
    if (justPressed3 != pressed3){
    if(addr == 254){stopPlayback();pressed3 = 255;statusChange=1;playChange=1;}        
    if(addr == 253){startPlayback(); pressed3 = 255;statusChange=1;playChange=1;}
    if(addr == 251){currentlyEditting=1; pressed3 = 255;partChange=1;statusChange=1;} // part1
    if(addr == 247){currentlyEditting=2; pressed3 = 255;partChange=1;statusChange=1;} // part2
    if(addr == 239){currentlyEditting=3; pressed3 = 255;partChange=1;statusChange=1;} // part3
    if(addr == 223){currentlyEditting=4; pressed3 = 255;partChange=1;statusChange=1;} // part4
    if(addr == 191){editMode= !editMode; pressed3 = 255;} // toggle editmode
    if(addr == 127){button[16]= !button[16]; pressed3 = 255;}
    }
  }
    if(buttSet==4){
    if (justPressed4 != pressed4){
    if(addr == 254){ 
      if(menu==0){menu=1;}
      else if(menu==1){menu=0;exitMenu=1;statusChange=1; }
      statusChange=1;
      pressed4 = 255;
      value=0;} //top button MENU/exit
    if(addr == 253){enter=1;pressed4 = 255;} //down button ENTER
    if(addr == 251){setBpm(0);value--;statusChange=1;  pressed4 = 255;} //left button
    if(addr == 247){setBpm(1);value++;statusChange=1;   pressed4 = 255;} //right button
    if(addr == 239){lcd.clear();lcd.print("239");  pressed4 = 255;}
    if(addr == 223){lcd.clear();lcd.print("223");  pressed4 = 255;}
    if(addr == 191){lcd.clear();lcd.print("191");  pressed4 = 255;}
    if(addr == 127){lcd.clear();lcd.print("127");  pressed4 = 255;}
    }
  }
    
}

void setLed(byte nr, bool state)
{
  shiftLed.setLed(nr,state);
}


void writeToLeds()
{
  if(partPart==0){setLed(25,0);setLed(22,1);}
  if(partPart==1){setLed(22,0);setLed(23,1);}
  if(partPart==2){setLed(23,0);setLed(24,1);}
  if(partPart==3){setLed(24,0);setLed(25,1);}
  byte a = 0;

  if(editMode==1){ // shows currently active notes
    for(byte i=0; i<16; i++){
      for(byte a=0; a<=3; a++){
        if(notesStatus[currentlyEditting-1][i+currentPartEdit*16]==1){setLed(i,1);}
        if(notesStatus[currentlyEditting-1][i+currentPartEdit*16]==0){setLed(i,0);}
        }                                         
    }
  }// shows currently active notes 
}

void playNote(byte noteNr, bool state)
{
  
  if(state == 1){ //playing note on 
 
  
  if(partPart==0) {setLed((noteNr), 1);} // to make the seqleds play the partparts
  if(partPart==1) {setLed((noteNr-16), 1);} 
  if(partPart==2) {setLed((noteNr-32), 1);}
  if(partPart==3) {setLed((noteNr-48), 1);} // dus dat
  
  for(byte a=0; a<=3; a++){if(currentlyPlaying[0]==1 && notesStatus[a][noteNr]==1 ){MIDI.sendNoteOn(notes[a][noteNr],potPositions[a],a+1);}}

  
  } //play  on end
  if(state == 0 ){ //playnote off
  
  if(partPart==0) {setLed((noteNr), 0);} // to make the seqleds play the partparts
  if(partPart==1) {setLed((noteNr-16), 0);} // to make the seqleds play the partparts
  if(partPart==2) {setLed((noteNr-32), 0);}
  if(partPart==3) {setLed((noteNr-48), 0);} // dus dat
  
  for(byte a=0; a<=3; a++){if(currentlyPlaying[0]==1 && notesStatus[a][noteNr]==1 ){MIDI.sendNoteOff(notes[a][noteNr],0,a+1);}}
    
} //playnote off end
}
  
void notes_init()
{
  for(byte a=0; a <=3; a++){  
    for(byte i=0; i<64; i++){
      notes[a][i]=0;
      notesStatus[a][i]=0;
    }
  }
}

//void setNote()
//{
//  for(int a=1; a<5; a++){ // for looping thru patches
//    int i = 0;
//    while(i<17)
//    {
//      if(button[i]==1 && notesStatus1[i]==1 ){notes1[i] = analogRead(A1)/45+40;}
//      if(button[i]==1 && digitalRead(7)==1 ){notes1[i] = 0; notesStatus1[i] = !notesStatus1[i];}
//      i++;
//    }
   
//  }// for looping thru patches

//}

void midiRecord()
{
}
  
void setNotes(byte currentlyEditting)
{
  byte currentSelectedNote = analogRead(A1)/45+40;
      for(byte i=0; i<16; i++)
      {
        for(byte a=0; a<=3; a++){
          if(currentPartEdit==a){if(button[i]==1){
            notes[currentlyEditting-1][i+a*16]=currentSelectedNote;
            notesStatus[currentlyEditting-1][i+a*16]= !notesStatus[currentlyEditting-1][i+a*16];
            button[i]= !button[i];}
          }
        }
      }
}
void startPlayback()
{
  justStarting=1; 
  noteNr=0;
  playing=1;
  //MIDI.sendRealTime(Stop);
  //MIDI.sendRealTime(Start);
}

void stopPlayback()
{
  playing=0;
  MIDI.sendRealTime(Stop);
}

void clearAfterEdit()
{
  for(byte i=0; i<16; i++)
  {
    button[i]=0;
  }
}

void showActiveNotes()
{
  if(editMode==1){
  
      for(byte a=0; a<=3; a++){for(byte i=0; i<16; i++){if(notesStatus[a][noteNr]==1){setLed(noteNr-a*16,1);}}}
   }
}


void getPositions()
{
  
  byte channel[] = {B10001111,B11001111,B10011111,B11011111,B10101111,B11101111,B10111111,B11111111};
  for(byte i=0; i<=7; i++){
    RB[0]=SPI.transfer(channel[i]);
    RB[1]=SPI.transfer(B00000000);
    RB[2]=SPI.transfer(B00000000);
    if(potPositions[i]= !RB[1]){potChanged[i]=1;}
    potPositions[i]=RB[1];
  }
} 
void sendCC()
{
  for(byte i=0; i<=7; i++){
    if(potChanged[i]==1){MIDI.sendControlChange(7,potPositions[i],1);}}
}
void showLevels()
{
  lcd.print("Levels:");
  lcd.setCursor(0,1);
  lcd.print("P1:");
  lcd.print(potPositions[0]/13);
  lcd.setCursor(4,1);
  lcd.print("P2:");
  lcd.print(potPositions[1]/13);
  lcd.setCursor(8,1);
  lcd.print("P3:");
  lcd.print(potPositions[2]/13);
  lcd.setCursor(12,1);
  lcd.print("P4:");
  lcd.print(potPositions[3]/13);
}

void showNoteStatus()
{
  lcd.clear();
  lcd.print(notesStatus[1][noteNr]);
}

void updateDisplay()
{
 if(statusChange==1){
      if((mainInit==1 && menu==0) || exitMenu==1){lcd.setCursor(8,0);lcd.print("bpm:");lcd.setCursor(0,1);lcd.print("part:");lcd.setCursor(8,1);lcd.print("patch:");mainInit=0;;}
      if((playing==1 && playChange==1 && menu==0) || exitMenu==1){lcd.setCursor(0,0);lcd.print("play    ");} else{lcd.setCursor(0,0);lcd.print("stop");playChange=0;}
      if((bpmChange==1 && menu==0) || exitMenu==1){lcd.setCursor(12,0);lcd.print(bpm);bpmChange=0;}
      if((partChange==1 && menu==0) || exitMenu==1){lcd.setCursor(5,1);lcd.print(currentlyEditting);partChange=0;}
      if((patchChange==1 && menu==0) || exitMenu==1){lcd.setCursor(14,1);lcd.print("A1");patchChange=0;}
      
      
      if(menu==1){
        lcd.clear();
        lcd.print("Menu");
        lcd.setCursor(0,1);
        currentMenuItem=value;
        lcd.print(menuItem[currentMenuItem]);
        if(enter=1){settings(currentMenuItem);enter=0;}
        
        //menu=0;
      }
      statusChange=0;
   }
}

void settings(byte currentMenuItem)
{
  if(currentMenuItem=0){
    lcd.setCursor(8,1);
    lcd.print(bpm);
    
  }
}

void setBpm(byte incrDecr)
{
  //value=bpm;
  
  if(incrDecr==1){bpm++;convertToMs();}
  if(incrDecr==0){bpm--;convertToMs();}
  statusChange=1;
  bpmChange=1;
}
void convertToMs()
{
  SPEED=(60000/4)/bpm;
}
void convertToBpm()
{
   bpm= 60000/(SPEED*4);
}
  
