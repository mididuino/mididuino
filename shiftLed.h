/* shitLed library
 
  This file is part of the shiftLed arduino library.
  it is licensed under the publice domain license, everyone is allowed and encouraged to use, modify, and redistribute this library
  The pin vars need to be changed in shiftLed.cpp accoring to the pins you wish to use for your shift registers.
  The 74hhc595 datasheet is available at http://www.nxp.com/documents/data_sheet/74HC_HCT595.pdf
  this is is the shiftLed header file */


#ifndef shiftLed_h
#define shiftLed_h

#include "Arduino.h"

class shiftLED
{
  public:
    shiftLED();
    void setLed(uint8_t ledNumber, bool state);
};

#endif
