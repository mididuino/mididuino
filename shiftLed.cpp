/* shitLed library
  
 This file is part of the shiftLed arduino library.
 it is licensed under the publice domain license, everyone is allowed and encouraged to use, modify, and redistribute this library
 The pin vars need to be changed in shiftLed.cpp accoring to the pins you wish to use for your shift registers.
 The 74hhc595 datasheet is available at http://www.nxp.com/documents/data_sheet/74HC_HCT595.pdf
 this is is the shiftLed cpp file */

#include "Arduino.h"
#include "shiftLed.h"
const uint8_t dataPin = 10; //data and latchpin of the first shift register
const uint8_t latchPin = 9;
const uint8_t clockPin = 8; // clock pin

volatile uint8_t tempByte0;
volatile uint8_t tempByte1;
byte bytes[] =  {1,2,4,8,16,32,64,128 };


shiftLED::shiftLED()
{
	pinMode(dataPin, OUTPUT);     
	pinMode(latchPin, OUTPUT);
	pinMode(clockPin, OUTPUT);   
}

void shiftLED::setLed(uint8_t ledNumber, bool state)
{
  	if(ledNumber <=8){  
  	  if(state==1 && (tempByte0&bytes[ledNumber-1])==0 ){tempByte0=tempByte0+bytes[ledNumber-1];}
	  if(state==0 && (tempByte0&bytes[ledNumber-1])!=0 ){tempByte0=tempByte0-bytes[ledNumber-1];}
	  digitalWrite(latchPin, LOW);
 	  shiftOut(dataPin, clockPin, MSBFIRST, tempByte1);
	  shiftOut(dataPin, clockPin, MSBFIRST, tempByte0);
 	  digitalWrite(latchPin, HIGH);
   }
       if(ledNumber > 8 ){  
    	  if(state==1 && (tempByte1&bytes[ledNumber-9])==0 ){tempByte1=tempByte1+bytes[ledNumber-9];}
	  if(state==0 && (tempByte1&bytes[ledNumber-9])!=0 ){tempByte1=tempByte1-bytes[ledNumber-9];}
	  digitalWrite(latchPin, LOW);
	  shiftOut(dataPin, clockPin, MSBFIRST, tempByte1);
	  shiftOut(dataPin, clockPin, MSBFIRST, tempByte0);
	  digitalWrite(latchPin, HIGH);
   }
}
