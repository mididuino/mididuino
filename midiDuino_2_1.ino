#include <SDCARD.h>
#include <MsTimer2.h>
#include <shiftLed.h>
#include <I2CIO.h>
#include <LiquidCrystal.h>
#include <LiquidCrystal_I2C.h>
#include <LCD.h>x
#include <FastIO.h>
#include <MemoryFree.h>
#include <SPI.h>
#include <MIDI.h>
#include <Wire.h>
//#include <SDCARDmodded.h>
LiquidCrystal_I2C lcd(0x3B);

byte tempSpeed;
byte currentPatch=1;
byte menuItemValue;
byte midiCh[8]={1,2,3,4,5,6,7,8};
byte pressed1;
byte pressed2;
byte pressed3;
byte pressed4;
byte midiInCh = 4;
byte SPEED=140;
byte button[16];
int notesStatus[8][4];
byte currentSelectedNote = 0;
byte value=1; //interface button values 
byte lastValue;
byte potChanged[7];
bool playChange=1;
bool bpmChange=1;
bool partChange=1;
bool patchChange=1;
bool mainInit=1;
bool menu=0;
bool exitMenu=0;
bool enter=0;
bool itemActive=0; //bool to store wheter a menu item is active 
bool record=0;
char* menuItem[5]={"MidiCH","Fadermode","MIDI-mode","Del patch","BPM"};
char* noteNames[12]={"C","C#","D","D#","E","F","F#","G","G#","A","A#","B"};

byte currentMenuItem=0;
int BPM;
bool bpmSet=0;
bool statusChange=0;
byte currentlyEditting=1; //patch currently editing
byte currentPartEdit = 0;

byte octave=0;
byte partPart = 0;
byte RB[3]; // recieve bits for spi
byte potPositions[7]; // position values of faders
byte justPressed1=0; // var to check if a butt has just been pressed to prevent data spam
byte justPressed2=0; 
byte justPressed3=0; 
byte justPressed4=0;
bool playing = 0;
byte justStarting=1;
byte currentNote;
long waitUntillNote = 0;
long waitUntillRead = 0;
long waitUntillSyncPulse = 0;
long waitUntillUpdate = 0; 
byte noteNr=0;
byte pulseCount=0;
bool transportState = 0;
bool editMode = 0;
int benchmark=0;
shiftLED shiftLed;

unsigned char buffer[512]; //var thats holds the notes
unsigned long sector = 300  ; // sd card sector
int error = 0; //var to store errors from sd read/write


void setup()
{

  SPI.begin();
  SPI.setBitOrder(MSBFIRST);
  SPI.setClockDivider(2);  
 
  lcd.begin(16,2);
  lcd.print(freeMemory()); 
  pinMode(2,OUTPUT);//record led
  pinMode(3,OUTPUT);//midi note in led
  pinMode(4,OUTPUT);//CS for the adc
  digitalWrite(4,HIGH); //disable the max 186 adc

  MIDI.begin(MIDI_CHANNEL_OMNI);
  MIDI.setHandleNoteOn(HandleNoteOn);
  MIDI.setHandleNoteOff(HandleNoteOff);
  MIDI.setHandleProgramChange(handleProgramChange);  
   
  Serial.begin(115200); // start out in usb midi mode
  Wire.begin();        // join i2c bus (address optional for master)
  MIDI.sendRealTime(Stop);
  loadPatch(value);
  MsTimer2::set(SPEED/8,clockOut);
   
}

void loop()
{
 
  MIDI.read();
  convertToBpm();  
  bool noteOn = 0;
    readInput(3); //read and convert transport and part select buttons
    convertInput(pressed3, 3);   
  readInput(1);
  if(editMode==0){playInput(pressed1, 1);}
  readInput(2);
  if(editMode==0){playInput(pressed2, 2);}
  readInput(3); //read and convert transport and part select buttons
  convertInput(pressed3, 3); 
  readInput(4); //read and convert transport and part select buttons
  convertInput(pressed4, 4);  
  updateDisplay(); 
  
  while(playing==1){    
      MIDI.read(); 
      getPositions();
      byte tmp=currentlyEditting;
      if(currentlyEditting != tmp){statusChange=1;}
      readInput(1); //read and convert seq butts 1-8
      if(editMode==1){setNotes(currentlyEditting);}
      if(editMode==1){convertInput(pressed1, 1);}
      if(editMode==0){playInput(pressed1, 1);}
      readInput(2); //read and convert seq butts 9-16
 //  if(editMode==1){setNotes(currentlyEditting);}
      if(editMode==1){convertInput(pressed2, 2);}
      if(editMode==0){playInput(pressed2, 2);}
      readInput(3); //read and convert transport and part select buttons
      convertInput(pressed3, 3); 
      readInput(4); //read and convert transport and part select buttons
      convertInput(pressed4, 4);     
   // if(editMode==1){setNotes(currentlyEditting);}
      if(editMode==0){clearAfterEdit();}
      writeToLeds();
      updateDisplay();
      if (noteNr==64){noteNr=0;}
      partPart=noteNr/16;
      if(noteNr%16==0 && menu==0){statusChange=1;partChange=1;}
      checkPatch();
      if (partPart>3){partPart=0;}
  }
      
}

void readInput(byte buttSet)
{
  
    if(buttSet == 1){
      Wire.requestFrom(0x39, 1);
      byte c = Wire.read(); // receive a byte as character
      if(c < 255) {pressed1 = c;}
      justPressed1 = c;
    }
    if(buttSet == 2){
      Wire.requestFrom(0x38, 1);
      byte c = Wire.read(); // receive a byte as character
      if(c < 255) {pressed2 = c;}
      justPressed2 = c;
    }
    if(buttSet == 3){
      Wire.requestFrom(0x3A, 1);
      byte c = Wire.read(); // receive a byte as character
      if(c < 255) {pressed3 = c;}
      justPressed3 = c;
    }
   if(buttSet == 4){
      Wire.requestFrom(0x3C, 1);
      byte c = Wire.read(); // receive a byte as character
      if(c < 255) {pressed4 = c;}
      justPressed4 = c;
    }

}

void playInput(byte addr, byte buttSet)
{
   if(buttSet==1){
    if (justPressed1 != pressed1){
    if(addr == 239){pressed1 = 255;currentSelectedNote=0+octave*12;statusChange=1;MIDI.sendNoteOn(currentSelectedNote,127,1);if(record==1){setNoteS(currentlyEditting-1, noteNr,currentSelectedNote);setNotesStatus(currentlyEditting-1,noteNr,1);}}
    if(addr == 253){pressed1 = 255;currentSelectedNote=1+octave*12;statusChange=1;MIDI.sendNoteOn(currentSelectedNote,127,1);if(record==1){setNoteS(currentlyEditting-1, noteNr,currentSelectedNote);setNotesStatus(currentlyEditting-1,noteNr,1);}}
    if(addr == 191){pressed1 = 255;currentSelectedNote=2+octave*12;statusChange=1;MIDI.sendNoteOn(currentSelectedNote,127,1);if(record==1){setNoteS(currentlyEditting-1, noteNr,currentSelectedNote);setNotesStatus(currentlyEditting-1,noteNr,1);}}
    if(addr == 247){pressed1 = 255;currentSelectedNote=3+octave*12;statusChange=1;MIDI.sendNoteOn(currentSelectedNote,127,1);if(record==1){setNoteS(currentlyEditting-1, noteNr,currentSelectedNote);setNotesStatus(currentlyEditting-1,noteNr,1);}}
    if(addr == 254){pressed1 = 255;currentSelectedNote=4+octave*12;statusChange=1;MIDI.sendNoteOn(currentSelectedNote,127,1);if(record==1){setNoteS(currentlyEditting-1, noteNr,currentSelectedNote);setNotesStatus(currentlyEditting-1,noteNr,1);}}
    if(addr == 223){pressed1 = 255;currentSelectedNote=5+octave*12;statusChange=1;MIDI.sendNoteOn(currentSelectedNote,127,1);if(record==1){setNoteS(currentlyEditting-1, noteNr,currentSelectedNote);setNotesStatus(currentlyEditting-1,noteNr,1);}}
    if(addr == 251){pressed1 = 255;currentSelectedNote=6+octave*12;statusChange=1;MIDI.sendNoteOn(currentSelectedNote,127,1);if(record==1){setNoteS(currentlyEditting-1, noteNr,currentSelectedNote);setNotesStatus(currentlyEditting-1,noteNr,1);}}
    if(addr == 127){pressed1 = 255;currentSelectedNote=7+octave*12;statusChange=1;MIDI.sendNoteOn(currentSelectedNote,127,1);if(record==1){setNoteS(currentlyEditting-1, noteNr,currentSelectedNote);setNotesStatus(currentlyEditting-1,noteNr,1);}}
    }
  }
  if(buttSet==2){
    if (justPressed2 != pressed2){
    if(addr == 127){pressed2 = 255;currentSelectedNote=8+octave*12;statusChange=1;MIDI.sendNoteOn(currentSelectedNote,127,1);if(record==1){setNoteS(currentlyEditting-1, noteNr,currentSelectedNote);setNotesStatus(currentlyEditting-1,noteNr,1);}}
    if(addr == 191){pressed2 = 255;currentSelectedNote=9+octave*12;statusChange=1;MIDI.sendNoteOn(currentSelectedNote,127,1);if(record==1){setNoteS(currentlyEditting-1, noteNr,currentSelectedNote);setNotesStatus(currentlyEditting-1,noteNr,1);}}
    if(addr == 223){pressed2 = 255;currentSelectedNote=10+octave*12;statusChange=1;MIDI.sendNoteOn(currentSelectedNote,127,1);if(record==1){setNoteS(currentlyEditting-1, noteNr,currentSelectedNote);setNotesStatus(currentlyEditting-1,noteNr,1);}}
    if(addr == 239){pressed2 = 255;currentSelectedNote=11+octave*12;statusChange=1;MIDI.sendNoteOn(currentSelectedNote,127,1);if(record==1){setNoteS(currentlyEditting-1, noteNr,currentSelectedNote);setNotesStatus(currentlyEditting-1,noteNr,1);}}
    if(addr == 247){pressed2 = 255;currentSelectedNote=0+(octave+1)*12;statusChange=1;MIDI.sendNoteOn(currentSelectedNote,127,1);if(record==1){setNoteS(currentlyEditting-1, noteNr,currentSelectedNote);setNotesStatus(currentlyEditting-1,noteNr,1);}}
    if(addr == 251){pressed2 = 255;currentSelectedNote=1+(octave+1)*12;statusChange=1;MIDI.sendNoteOn(currentSelectedNote,127,1);if(record==1){setNoteS(currentlyEditting-1, noteNr,currentSelectedNote);setNotesStatus(currentlyEditting-1,noteNr,1);}}
    if(addr == 253){pressed2 = 255;currentSelectedNote=2+(octave+1)*12;statusChange=1;MIDI.sendNoteOn(currentSelectedNote,127,1);if(record==1){setNoteS(currentlyEditting-1, noteNr,currentSelectedNote);setNotesStatus(currentlyEditting-1,noteNr,1);}}
    if(addr == 254){pressed2 = 255;currentSelectedNote=3+(octave+1)*12;statusChange=1;MIDI.sendNoteOn(currentSelectedNote,127,1);if(record==1){setNoteS(currentlyEditting-1, noteNr,currentSelectedNote);setNotesStatus(currentlyEditting-1,noteNr,1);}}
    }
  }
}
void convertInput(byte addr, byte buttSet) // converts button adres to button number 
{
  if(buttSet==1){
    if (justPressed1 != pressed1){
    if(addr == 239){button[1]= !button[1]; pressed1 = 255;}
    if(addr == 253){button[2]= !button[2]; pressed1 = 255;}
    if(addr == 191){button[3]= !button[3]; pressed1 = 255;}
    if(addr == 247){button[4]= !button[4]; pressed1 = 255;}
    if(addr == 254){button[5]= !button[5]; pressed1 = 255;}
    if(addr == 223){button[6]= !button[6]; pressed1 = 255;}
    if(addr == 251){button[7]= !button[7]; pressed1 = 255;}
    if(addr == 127){button[8]= !button[8]; pressed1 = 255;}
    }
  }
  if(buttSet==2){
    if (justPressed2 != pressed2){
    if(addr == 127){button[9]= !button[9]; pressed2 = 255;}
    if(addr == 191){button[10]= !button[10]; pressed2 = 255;}
    if(addr == 223){button[11]= !button[11]; pressed2 = 255;}
    if(addr == 239){button[12]= !button[12]; pressed2 = 255;}
    if(addr == 247){button[13]= !button[13]; pressed2 = 255;}
    if(addr == 251){button[14]= !button[14]; pressed2 = 255;}
    if(addr == 253){button[15]= !button[15]; pressed2 = 255;}
    if(addr == 254){button[16]= !button[16]; pressed2 = 255;}
    }
  }
  if(buttSet==3){
    if (justPressed3 != pressed3){
    if(addr == 254){stopPlayback();pressed3 = 255;statusChange=1;playChange=1;}        
    if(addr == 253){startPlayback(); pressed3 = 255;statusChange=1;playChange=1;}
    if(addr == 251 && menu==0){if(currentlyEditting==1){currentlyEditting=9;}currentlyEditting--; pressed3 = 255;partChange=1;patchChange=1;value=currentPatch;statusChange=1;} // part1
    if(addr == 247 && menu==0){if(currentlyEditting==8){currentlyEditting=0;}currentlyEditting++; pressed3 = 255;partChange=1;patchChange=1;value=currentPatch;statusChange=1;} // part2
    if(addr == 239 && menu==0){pressed3 = 255;lcd.setCursor(5,1);lcd.print("L");statusChange=1;} // part3
    if(addr == 223 && menu==0){pressed3 = 255;statusChange=1;} // part4
    if(addr == 191){if(editMode==1){clearLeds();}editMode= !editMode; pressed3 = 255;statusChange=1;} // toggle editmode
    if(addr == 127){button[16]= !button[16]; pressed3 = 255;statusChange=1;playChange=1;}
    }
  }
    if(buttSet==4){
    if (justPressed4 != pressed4){
    if(addr == 254){ 
      
      
      if(menu==0){lastValue=value;value=0;menu=1;}
      else if(menu==1){menu=0;exitMenu=1;statusChange=1; value=currentPatch;}
      statusChange=1;
      pressed4 = 255;
      } //top button MENU/exit
    if(addr == 253){enter=1;pressed4 = 255;statusChange=1;} //down button ENTER
    if(addr == 251){if(itemActive==0){if(menu==1&&value==0){value=5;}value--;}if(itemActive==1){menuItemValue--;} statusChange=1;pressed4 = 255;}
    if(addr == 247){if(itemActive==0){if(menu==1&&value==5){value=1;}value++;}if(itemActive==1){menuItemValue++;} statusChange=1;pressed4 = 255;} //right button
    if(addr == 239){if(editMode==1){
                      if(currentPartEdit==3){currentPartEdit=0;} else{currentPartEdit+=1;};statusChange=1; pressed4 = 255;}
                    if(editMode==0){
                    octave++;statusChange=1; pressed4 = 255;} 
                    }              
    if(addr == 223){if(editMode==1){
                      if(currentPartEdit==0){currentPartEdit=3;} else{currentPartEdit-=1;};statusChange=1;  pressed4 = 255;}
                    if(editMode==0){
                    octave--;statusChange=1; pressed4 = 255;}
                      }
    if(addr == 191){record = !record;  pressed4 = 255;}
    if(addr == 127){lcd.setCursor(5,1);lcd.print("S");savePatch();  pressed4 = 255;}
    }
  }
    
}

void setLed(byte nr, bool state)
{
  shiftLed.setLed(nr,state);
}


void writeToLeds()
{
   if(record==1){digitalWrite(2,HIGH);}
   if(record==0){digitalWrite(2,LOW);}
   if(editMode==1){ // shows currently active notes
    for(byte i=0; i<16; i++){
     // for(byte a=0; a<=3; a++){
        if(getNotesStatus(currentlyEditting-1,i+currentPartEdit*16)==1&& getNoteS(currentlyEditting-1,i+currentPartEdit*16)==currentSelectedNote){setLed(i,1);}
        if(getNotesStatus(currentlyEditting-1,i+currentPartEdit*16)==0){setLed(i,0);}
     //   }                                        
    }
  }// shows currently active notes */
}

void playNote(byte noteNr, bool state)
{
  
  if(state == 1){ //playing note on 
 

  if(partPart==0) {setLed((noteNr), 1);} // to make the seqleds play the partparts
  if(partPart==1) {setLed((noteNr-16), 1);} 
  if(partPart==2) {setLed((noteNr-32), 1);}
  if(partPart==3) {setLed((noteNr-48), 1);} // dus dat
  
  for(byte a=0; a<=7; a++){if(getNotesStatus(a,noteNr) != 0){if(getNoteS(a,noteNr) != 0){MIDI.sendNoteOn(getNoteS(a,noteNr),potPositions[a],midiCh[a]);}}}
  
  
  } //play  on end
  if(state == 0 ){ //playnote off
 
  if(partPart==0) {setLed((noteNr), 0);} // to make the seqleds play the partparts
  if(partPart==1) {setLed((noteNr-16), 0);} // to make the seqleds play the partparts
  if(partPart==2) {setLed((noteNr-32), 0);}
  if(partPart==3) {setLed((noteNr-48), 0);} // dus dat
  
 // for(byte a=0; a<=3; a++){if(notesStatus[a][noteNr]==1 ){MIDI.sendNoteOff(notes[a][noteNr],0,midiCh[a]);}}
    
} //playnote off end
}
  
void notes_init()
{
  for(byte a=0; a <=7; a++){  
    for(byte i=0; i<64; i++){
    //  setNoteS(a,i,0);
       setNotesStatus(a,i,1);
    }
  }
    //SDCARD.writeblock(sector); 
}


void midiRecord()
{
}
  
void setNotes(byte currentlyEditting)
{   
      for(byte i=0; i<16; i++)
      {
        for(byte a=0; a<=3; a++){
          if(currentPartEdit==a){if(button[i]==1){
            setNoteS(currentlyEditting-1,i+a*16,currentSelectedNote);
            if(getNotesStatus(currentlyEditting-1,i+a*16)==1){setNotesStatus(currentlyEditting-1,i+a*16,0);setNoteS(currentlyEditting,i+a*16,0);setNotesStatus(currentlyEditting,i+a*16,0);}
            else{setNotesStatus(currentlyEditting-1,i+a*16,1);}
            button[i]= !button[i];}
          }
        }
      }
}
void startPlayback()
{
  justStarting=1; 
  noteNr=0;
  playing=1;
  pulseCount=0;
  MIDI.sendRealTime(Start);
  MsTimer2::start();
  
}
void clockOut()
{
  MIDI.sendRealTime(Clock);
    if(pulseCount==0){
      playNote(noteNr,0);
      noteNr++;      
      playNote(noteNr,1);}  
     pulseCount++;
   //  getPositions();
    if(pulseCount==6){pulseCount=0;} 
}

  
void stopPlayback()
{
  playing=0;
  MIDI.sendRealTime(Stop);
  MsTimer2::stop();
  
 // error = SDCARD.writeblock(sector); 
}

void clearAfterEdit()
{
  for(byte i=0; i<16; i++)
  {
    button[i]=0;
    setLed[i,0]; 
  }
}

void showActiveNotes()
{
  if(editMode==1){
  
      for(byte a=0; a<=3; a++){for(byte i=0; i<16; i++){if(getNotesStatus(a,noteNr)&& getNoteS(a,noteNr)==currentSelectedNote){setLed(noteNr-a*16,1);}}}
   }
}


void getPositions()
{
  digitalWrite(4,LOW);
  byte channel[] = {B10001111,B11001111,B10011111,B11011111,B10101111,B11101111,B10111111,B11111111};
  for(byte i=0; i<=7; i++){
    RB[0]=SPI.transfer(channel[i]);
    RB[1]=SPI.transfer(B00000000);
    RB[2]=SPI.transfer(B00000000);
    if(potPositions[i]= !RB[1]){potChanged[i]=1;}
     
    potPositions[i]=RB[1];
  }
  digitalWrite(4,HIGH);
} 
void sendCC()
{
  for(byte i=0; i<=7; i++){
    if(potChanged[i]==1){ ;}}
}
void showLevels()
{
  getPositions();
  lcd.print("Levels:");
  lcd.clear();
  lcd.print("P1:");
  lcd.print(potPositions[0]);
  delay(50); 
}


void updateDisplay()
{
 if(statusChange==1){
      if(exitMenu==1){mainInit=1;}
      if((mainInit==1 && menu==0) || exitMenu==1){lcd.setCursor(0,1);lcd.print(partPart);lcd.setCursor(8,1);lcd.print("bpm:");mainInit=0;;}
      if((playing==1 && playChange==1 && menu==0) || exitMenu==1){lcd.setCursor(0,0);lcd.print("Play");} else{lcd.setCursor(0,0);lcd.print("Stop");playChange=0;}
      if((bpmChange==1 && menu==0) || exitMenu==1){lcd.setCursor(12,1);lcd.print(BPM);bpmChange=0;}
      if((partChange==1 && menu==0) || exitMenu==1){lcd.setCursor(0,1);lcd.print(currentlyEditting);lcd.print("-");lcd.print(partPart+1);partChange=0;lcd.print("   ");}
      if((patchChange==1 && menu==0) || exitMenu==1){lcd.setCursor(12,0);lcd.print(">");lcd.setCursor(14,0);lcd.print(currentPatch);patchChange=0;}
      if(editMode==1){lcd.setCursor(0,0);lcd.print("Edit");lcd.setCursor(6,1); lcd.print(currentPartEdit+1);}
      lcd.setCursor(5,0);
      lcd.print(noteNames[currentSelectedNote%12]);
          
      lcd.setCursor(7,0);
      lcd.print(octave);
      lcd.print(" ");
    
      if(menu==1){
          lcd.clear();
          lcd.print("Menu");
          
        
          if(itemActive==0){
            
            currentMenuItem=value;        
            
          }
          lcd.setCursor(0,1);
          lcd.print(menuItem[currentMenuItem]);
          if(itemActive==1){settings(currentMenuItem);}
          if(enter==1){enter=0;settings(currentMenuItem);itemActive=1;}
        }
      
      //if(itemActive==1){settings(currentMenuItem);}
      statusChange=0;
   }
}

void settings(byte currentMenuItem)
{
  if(currentMenuItem==1){
    lcd.setCursor(10,1);
    if(menuItemValue%2==0){lcd.print("Vol");}
    else{lcd.print("CC");}
  }
  
  if(currentMenuItem==2){
   lcd.setCursor(10,1);
   if(menuItemValue%2==0){lcd.print("usb");}
    else{lcd.print("midi");} 
   if(enter==1){if(menuItemValue%2==0){Serial.end(); Serial.begin(115200);itemActive=0;enter=0;}
                else{Serial.end(); Serial.begin(31250);itemActive=0;enter=0;}}                 
  }
  if(currentMenuItem==3){
    lcd.setCursor(10,1);
    if(menuItemValue%2==0){lcd.print("No");}
      else{lcd.print("Yes");} 
      if(enter==1){ if(menuItemValue%2==0){for(int i=0;i<512;i++){buffer[i]=0;savePatch();}}}
  }                                   
  if(currentMenuItem==4){
    if(bpmSet==0){menuItemValue=BPM;bpmSet=1;}
    lcd.setCursor(10,1);
    lcd.print(menuItemValue);
    if(enter==1){setBpm(menuItemValue);itemActive=0;enter=0;menuItemValue=0;bpmSet=0;lcd.setCursor(10,1);lcd.print( "   ");}
    
    
  }
}

void setBpm(byte bpm)
{
  BPM=bpm;
  convertToMs();
  MsTimer2::stop();
  MsTimer2::set(SPEED/8,clockOut);
  MsTimer2::start();
  statusChange=1;
}
void convertToMs()
{
  SPEED=(60000/3)/BPM;
}
void convertToBpm()
{
   BPM= 60000/(SPEED*3);
}

void HandleNoteOn(byte channel, byte pitch, byte velocity) 
{ 
  //MIDI.sendNoteOn(pitch,127,1);
  digitalWrite(3,HIGH);
  if(record){setNoteS(currentlyEditting-1,noteNr,pitch);setNotesStatus(currentlyEditting-1,noteNr,1);} 
}
  

void HandleNoteOff(byte channel, byte pitch, byte velocity) 
{
  digitalWrite(3,LOW);
  //lcd.clear();
}

void handleProgramChange(byte channel, byte number)
{
  currentlyEditting=number;
  statusChange=1;
  partChange=1;
}

void checkPatch()
{
   //currentPatch[1]=1;statusChange=1;patchChange=1;updateDisplay();
  if(menu==0){
    lcd.setCursor(10,0);
    lcd.print(value);
    if(noteNr>=63 && menu==0 && currentPatch != value){
      currentPatch=value;statusChange=1;patchChange=1;loadPatch(value);startPlayback();}
  }
}

void loadPatch(byte patchNum)
{
 SDCARD.readblock((sector+patchNum)-1);
 for(int i = 0; i<512;i++){
    if(buffer[i]==0){setNotesStatus(i/64,i-((i/64)*64)+1,0);} 
    if(buffer[i]> 0){setNotesStatus(i/64,i-((i/64)*64)+1,1);}
    }
  /* if(patchNum==0){
    for(byte i=0; i<=64; i++){
      setNoteS(currentlyEditting-1,i,0);
      setNotesStatus(currentlyEditting-1,i,0);
    }
  }
  */
}

void savePatch()
{
  digitalWrite(4,HIGH);
  SDCARD.writeblock((sector+value)-1); 
  //while(error != 0){ error = SDCARD.writeblock(sector); }
}
void clearLeds()
{
  for(byte i=0; i<16; i++){
    setLed[i,0];
  }
}

void setNotesStatus(byte part, byte noteNr, bool state)
{
  if(noteNr<17){
    if(state==1){bitWrite(notesStatus[part][1],noteNr,1);}
    if(state==0){bitWrite(notesStatus[part][1],noteNr,0);}
  }
  if(noteNr>16 && noteNr<33){
    if(state==1){bitWrite(notesStatus[part][2],noteNr-16,1);}
    if(state==0){bitWrite(notesStatus[part][2],noteNr-16,0);}
  }
  if(noteNr>32 && noteNr<49){
    if(state==1){bitWrite(notesStatus[part][3],noteNr-32,1);}
    if(state==0){bitWrite(notesStatus[part][3],noteNr-32,0);}
  }
  if(noteNr>48){
    if(state==1){bitWrite(notesStatus[part][4],noteNr-48,1);}
    if(state==0){bitWrite(notesStatus[part][4],noteNr-48,0);}
  }
  //set notes statusses
}

byte getNotesStatus(byte part,byte noteNr)
{
  if(noteNr<17){
    return bitRead(notesStatus[part][1],noteNr);}
  if(noteNr>16 && noteNr<33){
    return bitRead(notesStatus[part][2],noteNr-16);}
  if(noteNr>32 && noteNr<49){
    return bitRead(notesStatus[part][3],noteNr-32);}
  if(noteNr>48){
    return bitRead(notesStatus[part][4],noteNr-48);}
   
  //get notes statuses
}

byte getNoteS(byte part, byte noteNr)
{
   return buffer[((part*64)+noteNr)-1];
   //return number;
}

void setNoteS(byte part, byte noteNr, byte noteValue)
{
  buffer[((part*64)+noteNr)-1]=noteValue;
}
  
void demo(){
  for(byte i = 0; i<=16; i++){
    delay(50);
    setLed(i,1);
    delay(50);
    setLed(i,0);
  }
}
