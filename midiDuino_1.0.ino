#include <SPI.h>

#include <MIDI.h>
#include <tlc_config.h>
#include <Tlc5940.h>
#include <tlc_shifts.h>
#include <tlc_animations.h>
#include <tlc_fades.h>
#include <tlc_progmem_utils.h>
#include <tlc_servos.h>


#include <Wire.h>
#define LED_BRIGHTNESS 4069 //brightness of leds 0-4096
int midiChOne=1;
int midiChTwo=2;
int midiChThree=3;
int midiChFour=4;
byte pressed1;
byte pressed2;
byte pressed3;
int midiInCh = 4;
byte SPEED;
int button[16];
int notes1[16]; // note arrays for different patches
int notes2[16];
int notes3[16];
int notes4[16];
int notesStatus1[15]; // note status arrays for different patches
int notesStatus2[15];
int notesStatus3[15];
int notesStatus4[15];
int currentlyEditting; //patch currently editing
int currentlyPlaying[]= {1,1,1,1};

int justPressed1=0; // var to check if a butt has just been pressed to prevent data spam
int justPressed2=0; 
int justPressed3=0; 
bool playing = 0;
int justStarting=1;
int currentNote;
long waitUntillNote = 0;
long waitUntillRead = 0;
long waitUntillSyncPulse = 0;
long waitUntillUpdate = 0; 
int noteNr=0;
bool transportState = 0;

void setup()
{
  pinMode(7, INPUT);
  
  MIDI.begin(midiInCh);
  Tlc.init();
  Wire.begin();        // join i2c bus (address optional for master)
  //Serial.begin(9600);  // start serial for output
  //for(int i=0; i<15; i++){
    //notesStatus[i]=0;
  //}
  notes_init();
  MIDI.sendRealTime(Stop);
  //MIDI.sendRealTime(Start);
  startPlayback;
  currentlyPlaying[0]=0;
  currentlyPlaying[1]=0; 
  currentlyPlaying[3]=0;
  currentlyPlaying[4]=0;
}

void loop()
{
    
  bool noteOn = 0;
  readInput(3); //read and convert transport and part select buttons
  convertInput(pressed3, 3);   
  

  while(playing==1){
      currentlyEditting = analogRead(A2)/256+1;
      SPEED = analogRead(A0)/5;
      readInput(1); //read and convert seq butts 1-8
      convertInput(pressed1, 1); 
      readInput(2); //read and convert seq butts 9-16
      convertInput(pressed2, 2);
      readInput(3); //read and convert transport and part select buttons
      convertInput(pressed3, 3);     
      setNotes(currentlyEditting);
      if(millis() >= waitUntillUpdate){ // millis loop for outputting to leds
        waitUntillUpdate=millis()+200;
        writeToLeds();      
      }
      if(millis() >= waitUntillSyncPulse){ // millis loop for sync playing8 times as fast
          
          MIDI.sendRealTime(Clock);
         
          if(millis() >=waitUntillNote) { // millis loop for playing note
            
            if (noteOn == 1)
            {
              if(justStarting==1){MIDI.sendRealTime(Start);justStarting=0;}
              playNote(noteNr, 0);
              noteNr=noteNr+1;
              noteOn=0;
              
            }          
            if (noteOn == 0)
            {
              playNote(noteNr, 1);
              
              noteOn=1;
            }
            waitUntillNote=millis()+SPEED;
            
          if (currentlyEditting==1){setLed(17,1);}
          if (currentlyEditting==1){setLed(19,1);}   
          if (currentlyEditting==1){setLed(20,1);}   
          if (currentlyEditting==1){setLed(21,1);}   
          }
          int tempSpeed=SPEED/4;
          waitUntillSyncPulse=millis() + (SPEED-tempSpeed)/4;  
          if (currentlyEditting==1){setLed(17,0);}
          if (currentlyEditting==2){setLed(19,1);}   
          if (currentlyEditting==3){setLed(20,1);}   
          if (currentlyEditting==4){setLed(21,1);}   
      }
      
      if (noteNr==16){noteNr=0;}
  }
  
}

void readInput(int buttSet)
{
  
    if(buttSet == 1){
      Wire.requestFrom(0x38, 1);
      byte c = Wire.read(); // receive a byte as character
      if(c < 255) {pressed1 = c;}
      justPressed1 = c;
    }
    if(buttSet == 2){
      Wire.requestFrom(0x39, 1);
      byte c = Wire.read(); // receive a byte as character
      if(c < 255) {pressed2 = c;}
      justPressed2 = c;
    }
    if(buttSet == 3){
      Wire.requestFrom(0x3A, 1);
      byte c = Wire.read(); // receive a byte as character
      if(c < 255) {pressed3 = c;}
      justPressed3 = c;
    }
  

}


int convertInput(int addr, int buttSet) // converts button adres to button number 
{
  if(buttSet==1){
    if (justPressed1 != pressed1){
    if(addr == 254){button[1]= !button[1]; pressed1 = 255;}
    if(addr == 253){button[2]= !button[2]; pressed1 = 255;}
    if(addr == 251){button[3]= !button[3]; pressed1 = 255;}
    if(addr == 247){button[4]= !button[4]; pressed1 = 255;}
    if(addr == 239){button[5]= !button[5]; pressed1 = 255;}
    if(addr == 223){button[6]= !button[6]; pressed1 = 255;}
    if(addr == 191){button[7]= !button[7]; pressed1 = 255;}
    if(addr == 127){button[8]= !button[8]; pressed1 = 255;}
    }
  }
  if(buttSet==2){
    if (justPressed2 != pressed2){
    if(addr == 254){button[9]= !button[9]; pressed2 = 255;}
    if(addr == 253){button[10]= !button[10]; pressed2 = 255;}
    if(addr == 251){button[11]= !button[11]; pressed2 = 255;}
    if(addr == 247){button[12]= !button[12]; pressed2 = 255;}
    if(addr == 239){button[13]= !button[13]; pressed2 = 255;}
    if(addr == 223){button[14]= !button[14]; pressed2 = 255;}
    if(addr == 191){button[15]= !button[15]; pressed2 = 255;}
    if(addr == 127){button[16]= !button[16]; pressed2 = 255;}
    }
  }
  if(buttSet==3){
    if (justPressed3 != pressed3){
    if(addr == 254){stopPlayback();pressed3 = 255;}        
    if(addr == 253){startPlayback(); pressed3 = 255;}
    if(addr == 251){currentlyPlaying[0]= !currentlyPlaying[0]; pressed3 = 255;} // part1
    if(addr == 247){currentlyPlaying[1]= !currentlyPlaying[1]; pressed3 = 255;} // part2
    if(addr == 239){currentlyPlaying[2]= !currentlyPlaying[2]; pressed3 = 255;} // part3
    if(addr == 223){currentlyPlaying[3]= !currentlyPlaying[3]; pressed3 = 255;} // part4
    if(addr == 191){button[15]= !button[15]; pressed3 = 255;}
    if(addr == 127){button[16]= !button[16]; pressed3 = 255;}
    }
  }
    
}

void setLed(int nr, bool state)
{
  if(state == 1) {
    Tlc.set((nr-1), LED_BRIGHTNESS);
    Tlc.update();
  }
  if(state == 0) {
    Tlc.set((nr-1), 0);
    Tlc.update();
  }
}


void writeToLeds()
{
  for(int i=0; i < 16; i++)
  {
    if(button[i] == 1) { setLed(i, 1);}
    if(button[i] == 0) { setLed(i, 0);}
  }
  //for(int i=0; i < 6; i++)
  //{
  //  if(currentlyPlaying[i]==1){setLed(i+17,1);}
  //  if(currentlyPlaying[i]==0){setLed(i+17,0);}
  //}
  
}

void playNote(int noteNr, bool state)
{
  
  if(state == 1){ //playing note on 
  setLed(noteNr, LED_BRIGHTNESS);
  if(currentlyPlaying[0]==1){MIDI.sendNoteOn(notes1[noteNr],127,midiChOne);}
  if(currentlyPlaying[1]==1){MIDI.sendNoteOn(notes2[noteNr],127,midiChTwo);}
  if(currentlyPlaying[2]==1){MIDI.sendNoteOn(notes3[noteNr],127,midiChThree);}
  if(currentlyPlaying[3]==1){MIDI.sendNoteOn(notes4[noteNr],127,midiChFour);}
  } //play  on end
  if(state == 1 ){ //playnote off
  setLed(noteNr, 0);
  if(currentlyPlaying[0]==1){MIDI.sendNoteOff(notes1[noteNr],0,midiChOne);}
  if(currentlyPlaying[1]==1){MIDI.sendNoteOff(notes1[noteNr],0,midiChTwo);}
  if(currentlyPlaying[2]==1){MIDI.sendNoteOff(notes1[noteNr],0,midiChThree);}
  if(currentlyPlaying[3]==1){MIDI.sendNoteOff(notes1[noteNr],0,midiChFour);}
  } //playnote off end
}
  
void notes_init()
{
  for(int i=0; i<18; i++){
    notes1[i]=41;
    notesStatus1[i]=1;
  }
  for(int i=0; i<18; i++){
    notes2[i]=40;
    notesStatus2[i]=1;
  }
  for(int i=0; i<18; i++){
    notes3[i]=42;
    notesStatus3[i]=1;
  }
  for(int i=0; i<18; i++){
    notes4[i]=44;
    notesStatus4[i]=1;
  }
}

void setNote()
{
  for(int a=1; a<5; a++){ // for looping thru patches
    int i = 0;
    while(i<17)
    {
      if(button[i]==1 && notesStatus1[i]==1 ){notes1[i] = analogRead(A1)/45+40;}
      if(button[i]==1 && digitalRead(7)==1 ){notes1[i] = 0; notesStatus1[i] = !notesStatus1[i];}
      i++;
    }
   
  }// for looping thru patches

}
int setNotes(int currentlyEditting)
{
  int currentSelectedNote = analogRead(A1)/45+40;
  if(currentlyEditting==1)
  {
    for(int i=0; i<16; i++)
    {
      if(button[i]==1){notes1[i]=currentSelectedNote;}
    }
  }
  
  if(currentlyEditting==2)
  {
    for(int i=0; i<16; i++)
    {
      if(button[i]==1){notes2[i]=currentSelectedNote;}
    }
  }
  
  if(currentlyEditting==3)
  {
    for(int i=0; i<16; i++)
    {
      if(button[i]==1){notes3[i]=currentSelectedNote;}
    }
  }
  
  if(currentlyEditting==4)
  {
    for(int i=0; i<16; i++)
    {
      if(button[i]==1){notes4[i]=currentSelectedNote;}
    }
  }
  
}
void startPlayback()
{
  justStarting=1; 
  noteNr=0;
  playing=1;
  //MIDI.sendRealTime(Stop);
  //MIDI.sendRealTime(Start);
}

void stopPlayback()
{
  playing=0;
  MIDI.sendRealTime(Stop);
}

